package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JTextArea;

public class gui extends JFrame{
	JTextArea showtext;
	String str;
	public gui(){
		createFrame();
	}
	public void createFrame(){
		showtext = new JTextArea();
		setLayout(new BorderLayout());
		add(showtext, BorderLayout.NORTH);
	}
	public void setResult(String str,Double d){
		this.str =Double.toString(d);
		this.str = str+this.str;
		showtext.setText(this.str);
	}
	public void extendResult(String str,Double d){
		String s =Double.toString(d);
		this.str = this.str+ "\n"+str+s;
		showtext.setText(this.str);
	}
	
}
