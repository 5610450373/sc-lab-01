package test;

import gui.gui;
import model.Cashcard;

public class testcase {
	gui frame;

	public static void main(String[] args) {
		new testcase();
		//Cashcard C1 = new Cashcard();
		//System.out.println("Balance : "+C1.checkbalance());
		//C1.refill(500);
		//System.out.println("Balance : "+C1.checkbalance());
		//C1.pay(200);
		//System.out.println("Balance : "+C1.checkbalance());
	}
	public testcase(){
		frame = new gui();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		setTestCase();
	}
	
	public void setTestCase() {
		Cashcard C1 = new Cashcard();
		frame.setResult(C1.type,C1.checkbalance());
		C1.refill(500);
		frame.extendResult(C1.type,C1.num);
		C1.checkbalance();
		frame.extendResult(C1.type,C1.checkbalance());
		C1.pay(200);
		frame.extendResult(C1.type,C1.num);
		C1.checkbalance();
		frame.extendResult(C1.type,C1.checkbalance());
	}
	

}
